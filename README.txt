Definition of Done

1. The output should be an executable .sh script.
2. The tag must be in a form "DATE.COUNTER"
3. DATE must be in form "15.06.22"
4. COUNTER must be in form "001", "002", "003", etc.
5. When the script is called several times during the same day, the COUNTER must be increased.
6. The COUNTER must be reset back to 001 when the date has changed.
7. The script must output the new tag on the command line.
8. The new generated tag must be written in a text file named "current.tag" next to the script
9. If the text file is missing, the script must create it with a current tag.
10. We don't need to process the case when the tag is 999 (it is not likely to happen)
 
Example tags called subsequently:
15.06.22.001
15.06.22.002
15.06.22.003 
15.06.23.001
15.06.23.002
